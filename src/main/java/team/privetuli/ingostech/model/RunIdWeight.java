package team.privetuli.ingostech.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RunIdWeight implements Comparable<RunIdWeight> {
    private Long runId;
    private Double weight;

    @Override
    public int compareTo(RunIdWeight o) {
        return o.getWeight().compareTo(weight);
    }
}
