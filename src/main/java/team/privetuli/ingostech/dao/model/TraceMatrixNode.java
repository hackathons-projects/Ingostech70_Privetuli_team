package team.privetuli.ingostech.dao.model;

import lombok.Data;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

@Data
@NodeEntity(label = "TraceMatrixNode")
public class TraceMatrixNode {

    @GraphId
    private Long id;

    // Relations
    private TraceMatrixNode prevRunId;

    private TraceMatrixNode nextRunId;

    private TraceMatrixNode prevCompositeKey;

    private TraceMatrixNode nextCompositeKey;

    // Matrix metadata
    private Long runId;

    private String moduleName;

    private Long stringNumber;

    // Cell content
    private Boolean isCalled;

    private Double weight;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TraceMatrixNode that = (TraceMatrixNode) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "TraceMatrixNode{" +
                "id=" + id +
                ", runId=" + runId +
                ", moduleName='" + moduleName + '\'' +
                ", stringNumber=" + stringNumber +
                ", isCalled=" + isCalled +
                ", weight=" + weight +
                '}';
    }
}
