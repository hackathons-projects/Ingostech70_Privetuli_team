package team.privetuli.ingostech.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import team.privetuli.ingostech.model.dto.ExecutionResultDTO;
import team.privetuli.ingostech.service.TestDataService;

import java.util.Map;

@Controller
public class TraceController {

    private final TestDataService testDataService;

    @Autowired
    public TraceController(TestDataService testDataService) {
        this.testDataService = testDataService;
    }

    @GetMapping("/ping")
    @ResponseBody
    public ResponseEntity<String> ping() {
        return ResponseEntity.ok("ALIVE");
    }

    @GetMapping("/run")
    public String run(Map<String, Object> model) {
        ExecutionResultDTO result = testDataService.run();
        model.put("coverage", result.getCoverage());
        model.put("runIds", result.getMethodsRunIds());
        return "run";
    }
}
