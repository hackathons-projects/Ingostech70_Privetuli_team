package team.privetuli.ingostech;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import team.privetuli.ingostech.service.TestDataServiceImpl;

@SpringBootApplication
public class IngostechApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = new SpringApplicationBuilder(IngostechApplication.class).headless(false).run(args);
	}
}
